import re
import time
from PyQt5 import QtCore
from app_setting import AppSetting
from qc.security.security import KeyDatabase
from qc.enum_color import ColorResult
from autotests.device_tests.abstract_test_view import AbstractTestView
from autotests.utils import db_handler
from __version__ import __version__


class TestFirePlus(AbstractTestView):
    AppSetting.DEBUG = True

    dev_type = 'fire_protect_plus'
    dev_id = db_handler.devices[dev_type][0][:6]
    dev_type_num = db_handler.devices[dev_type][0][-3:-1]

    database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)

    def test_success_device_reg(self, qtbot, mock_socket_csa):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='00')

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "00")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Незапилена"

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Зареєструвати'
        # time.sleep(2)

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='05')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)
        _, grade, _, info, defects, time_reg, success, operator, qr = \
            self.database.get_stat_device_after_success_reg(qr=db_handler.devices[self.dev_type][0])

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

    def test_scan_device_without_prog(self, qtbot, mock_socket_csa):
        self.scan_without_prog(qtbot, mock_socket_csa, self.database)

    def test_scan_device_without_assembling(self, qtbot, mock_socket_csa):
        self.scan_without_assembling(qtbot, mock_socket_csa, self.database)

    def test_scan_device_without_calibration(self, qtbot, mock_socket_csa):
        self.scan_without_calibration(qtbot, mock_socket_csa, self.database)

    def test_scan_device_without_long_test(self, qtbot, mock_socket_csa):
        self.scan_without_long_test(qtbot, mock_socket_csa, self.database)

    def test_device_with_qc_passed(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_retry_test_popup_true):
        main_w, *_ = mock_socket_csa
        self.scan_device_with_qc_passed(qtbot, main_w, self.database)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 1) is not None

    def test_success_device_reg_with_grade_defects(self, qtbot, mock_socket_csa, mock_defects_grade):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        self.add_grade_defects(qtbot=qtbot, main_w=main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "00")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Незапилена"

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Зареєструвати'
        # time.sleep(2)

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)
        _, grade, _, info, defects, time_reg, success, operator, qr = \
            self.database.get_stat_device_after_success_reg(qr=db_handler.devices[self.dev_type][0])

        assert grade == '1'
        assert __version__ in info
        assert defects == '"Сколи;Прожоги"'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

    def test_tamper_fail(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "00")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Незапилена"

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Повернути'
        # time.sleep(2)

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == ";Тампер"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None

    def test_autotest_fail(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "00")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Незапилена"

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Повернути'
        # time.sleep(2)

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == ";Сенсорна кнопка"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None

    def test_camera_dirty_fail(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "01")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Запилена"

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Повернути'
        # time.sleep(2)

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == ";Камера, запиленість"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None

    def test_camera_dirty_no_status(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "02")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Невідомо",
                        timeout=10 * 1000)

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Повернути'
        # time.sleep(2)

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == ";Камера, запиленість"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None

    def test_all_func_fail(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert "Тампер" in reason
        assert "Сенсорна кнопка" in reason
        assert "Камера, запиленість" in reason
        assert "Рівень батареї" in reason
        assert "Додаткова батарея" in reason
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None

    def test_battery_fail(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "00")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Незапилена"

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0062")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '98'
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).isEnabled()
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Повернути'
        time.sleep(2)

        # test fail register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == ";Рівень батареї"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None

    def test_res_battery_fail(self, qtbot, mock_socket_csa, mock_you_sure):
        main_w, server, socket = mock_socket_csa
        socket.dev = (self.dev_id, self.dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=self.database, programming=True, assembling=True,
                                   calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=18)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (self.dev_id, self.dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (self.dev_id, self.dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check button alarm
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.isChecked()
        main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check dirty cam
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("38", "00")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).label_status.text() == "Незапилена"

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check res batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("34", "0062")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).label_status.text() == '98'
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (self.dev_id, self.dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).isEnabled()
        assert not main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[18].cellWidget(0, 9).text() == 'Повернути'
        time.sleep(2)

        # test fail register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[18].cellWidget(0, 9),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[18].cellWidget(0, 9) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (self.dev_id, self.dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (self.dev_id, self.dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = self.database.get_stat_device_after_fail_reg(qr=db_handler.devices[self.dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[self.dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == ";Додаткова батарея"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 9
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[self.dev_type][0]
        assert repair_type is None
