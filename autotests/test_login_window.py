import re
from PyQt5 import QtCore
from app_setting import AppSetting


class TestLoginWidget:
    AppSetting.DEBUG = True

    def test_scanner_combobox(self, qtbot, mock_login_w_scanner_ports, login_widget):
        assert login_widget.combo_box_ports.isEnabled()
        assert login_widget.combo_box_ports.currentText() is not None

    def test_scanner_port_connect(self, qtbot, login_widget):
        qtbot.mouseClick(login_widget.push_btn_connect_port, QtCore.Qt.LeftButton)
        assert not login_widget.combo_box_ports.isEnabled()
        assert login_widget.push_btn_connect_port.text() == 'Відключитися'

        qtbot.mouseClick(login_widget.push_btn_connect_port, QtCore.Qt.LeftButton)
        assert login_widget.combo_box_ports.isEnabled()
        assert login_widget.push_btn_connect_port.text() == 'Підключитися'
        qtbot.mouseClick(login_widget.push_btn_connect_port, QtCore.Qt.LeftButton)

    def test_scanner_qr_login_without_db_access(self, qtbot, mock_db_authorize_raise_operational_error, login_widget):
        with qtbot.waitSignal(signal=login_widget.thread_authorize.signal_failure_auth, timeout=10 * 1000):
            login_widget._search_login_operator_qr_code('armen 123')

        assert login_widget.err_widget.label_message_text.text() == 'Перевірьте доступ до інтернету!'
        login_widget.err_widget.close()

    def test_scanner_qr_login_with_wrong_data_structure(self, qtbot, login_widget):
        login_widget._search_login_operator_qr_code('armen123')
        assert login_widget.err_widget.label_message_text.text() == 'QrCode не відповідає вимогам (Логін оператора ' \
                                                                        '-> пробіл -> Пароль оператора)'

    def test_success_scanner_qr_login(self, qtbot, mock_qr_scanner_login_widget, login_widget):
        with qtbot.waitSignal(signal=login_widget.signal_success_result, timeout=10 * 1000):
            mock_qr_scanner_login_widget.readyRead.emit()

    def test_login_without_username(self, qtbot, login_widget):
        login_widget.line_edit_login_password.setText('111')
        qtbot.mouseClick(login_widget.push_btn_login, QtCore.Qt.LeftButton)
        assert login_widget.err_widget.isActiveWindow()
        assert login_widget.err_widget.label_message_text.text() == 'Введіть логін'
        login_widget.line_edit_login_password.setText('')

    def test_login_without_password(self, qtbot, login_widget):
        login_widget.line_edit_login_name.setText('111')
        qtbot.mouseClick(login_widget.push_btn_login, QtCore.Qt.LeftButton)
        assert login_widget.err_widget.isActiveWindow()
        assert login_widget.err_widget.label_message_text.text() == 'Введіть пароль'
        login_widget.line_edit_login_password.setText('')

    def test_login_with_wrong_username(self, qtbot, login_widget):
        login_widget.line_edit_login_name.setText('wrong')
        login_widget.line_edit_login_password.setText('123')
        with qtbot.waitSignal(signal=login_widget.thread_authorize.signal_user_obj, timeout=10 * 1000):
            qtbot.mouseClick(login_widget.push_btn_login, QtCore.Qt.LeftButton)
        assert login_widget.err_widget.label_message_text.text() == 'Логін або пароль - некоректні!'

    def test_login_with_wrong_password(self, qtbot, login_widget):
        login_widget.line_edit_login_name.setText('armen')
        login_widget.line_edit_login_password.setText('wrong')
        with qtbot.waitSignal(signal=login_widget.thread_authorize.signal_user_obj, timeout=10 * 1000):
            qtbot.mouseClick(login_widget.push_btn_login, QtCore.Qt.LeftButton)
        assert login_widget.err_widget.label_message_text.text() == 'Логін або пароль - некоректні!'

    def test_success_manual_login(self, qtbot, login_widget):
        login_widget.line_edit_login_name.setText('armen')
        login_widget.line_edit_login_password.setText('123')
        with qtbot.waitSignal(signal=login_widget.signal_success_result, timeout=10 * 1000):
            qtbot.mouseClick(login_widget.push_btn_login, QtCore.Qt.LeftButton)
