import re
import time
import requests
from PyQt5 import QtCore
from app_setting import AppSetting


class TestMenuBar:
    AppSetting.DEBUG = True

    def test_add_hub_panel(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa,
                                         mock_qr_scanner_main_w):

        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # open add hub popup
        main_w.add_hub()

        assert main_w.widget_add_hub.isActiveWindow()
        assert main_w.widget_add_hub.line_edit_hub_qr.placeholderText() == 'qr code - хаба'
        # test positive add
        main_w.widget_add_hub.line_edit_hub_qr.setText("0003e-7bcd1-443f7-11100")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')
        qtbot.waitUntil(lambda: len(main_w.qc_main_widget.csa_client._hubs) > 1)
        assert '0003e7bc' in str(main_w.qc_main_widget.csa_client._hubs)
        # test hub already on acc
        main_w.add_hub()
        server.claim_hub_by_mk_answer = server.nak_answer
        main_w.widget_add_hub.line_edit_hub_qr.setText("0003e-7bcd1-443f7-11100")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)
        assert main_w.widget_add_hub.error_window.label_message_text.text() == "Цей хаб вже закріплений за вашим обліковим записом"

        main_w.qc_main_widget.control_hubs._delete_hub('0003e7bc')
        qtbot.waitUntil(lambda: len(main_w.qc_main_widget.csa_client._hubs) == 1)

        # test wrong qr
        main_w.widget_add_hub.line_edit_hub_qr.setText("123")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)
        assert main_w.widget_add_hub.error_window.label_message_text.text() == 'Не корекний qr code!'
        main_w.widget_add_hub.error_window.close()

        # test hub offline
        server.claim_hub_by_mk_answer = server.nak_answer
        main_w.widget_add_hub.line_edit_hub_qr.setText("0003e-7bcd1-443f7-11100")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)
        assert 'в офлайні' in main_w.widget_add_hub.error_window.label_message_text.text()
        main_w.widget_add_hub.error_window.close()

        # test when hub belongs to another user
        server.claim_hub_by_mk_answer = server.hub_belongs_to_another_user
        main_w.widget_add_hub.line_edit_hub_qr.setText("0003e-7bcd1-443f7-11100")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)
        qtbot.waitUntil(lambda: main_w.widget_add_hub.error_window.isActiveWindow(), timeout=5 * 1000)
        assert "вже приписаний до іншого користувача!" in main_w.widget_add_hub.error_window.label_message_text.text()
        main_w.widget_add_hub.error_window.close()

        # test wrong masterkey
        server.claim_hub_by_mk_answer = server.hub_wrong_masterkey
        main_w.widget_add_hub.line_edit_hub_qr.setText("0003e-7bcd1-443f7-11100")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)
        qtbot.waitUntil(lambda: main_w.widget_add_hub.error_window.isActiveWindow(), timeout=5 * 1000)
        assert main_w.widget_add_hub.error_window.label_message_text.text() == "Неправильний мастеркей!"
        main_w.widget_add_hub.error_window.close()

    def test_control_hub_panel(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa,
                                         mock_qr_scanner_main_w):
        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # test delete work hub
        main_w.qc_main_widget.control_hubs.show()
        main_w.qc_main_widget.control_hubs._delete_hub('0003e828')
        assert main_w.qc_main_widget.control_hubs._error_widget.label_message_text.text() == "Неможливо видалити робочий хаб! " \
                                                                               "Звернисть за допомогою до майстра участка!"

        # test delete not work hub
        main_w.add_hub()
        main_w.widget_add_hub.line_edit_hub_qr.setText("0003e-7bcd1-443f7-11100")
        qtbot.mouseClick(main_w.widget_add_hub.push_btn_add, QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: len(main_w.qc_main_widget.csa_client._hubs) > 1)
        assert '0003e7bc' in str(main_w.qc_main_widget.csa_client._hubs)

        main_w.qc_main_widget.control_hubs._delete_hub('0003e7bc')
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: len(main_w.qc_main_widget.csa_client._hubs) == 1)
        assert '0003e7bc' not in str(main_w.qc_main_widget.csa_client._hubs)

        # test arm/disarm work hub
        # TODO
        for i in range(main_w.qc_main_widget.control_hubs.main_layout.count()):
            hub = main_w.qc_main_widget.control_hubs.main_layout.itemAt(i).widget()

            qtbot.mouseClick(hub.push_btn_change_state, QtCore.Qt.LeftButton)
            socket.messages_heap.append(server.update_state_hub_arm())
            assert socket.check_message_exists(message_type='04', message_key='00')
            qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
            qtbot.waitUntil(lambda: hub._hub_obj.get_parameters("08") == '01', timeout=5 * 1000)
            assert hub.push_btn_change_state.text() == "зняти з охорони"

            qtbot.mouseClick(hub.push_btn_change_state, QtCore.Qt.LeftButton)
            assert socket.check_message_exists(message_type='04', message_key='02')
            socket.messages_heap.append(server.update_state_hub_disarm())
            qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
            qtbot.waitUntil(lambda: hub._hub_obj.get_parameters("08") == '00', timeout=5 * 1000)
            assert hub.push_btn_change_state.text() == "поставити на охорону"

    # TODO device control

    def test_change_frequency_work_hub(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa,
                                         mock_qr_scanner_main_w):
        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        for button in main_w.qc_main_widget.widget_changed_frequency.buttons_freq:
            if "RU" in button.text():
                freq_ru = button
                assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                                main_w.qc_main_widget.label_frequency_device.text()).group(0)

                qtbot.mouseClick(freq_ru, QtCore.Qt.LeftButton)
                assert socket.check_message_exists(message_type='0c', message_key='03')
                assert re.match(r'RU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                                main_w.qc_main_widget.label_frequency_device.text()).group(0)

    def test_change_frame_work_hub(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa,
                                         mock_qr_scanner_main_w):

        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # test change frame to 60s
        main_w.qc_main_widget.widget_change_frame_work_hub.show()
        main_w.qc_main_widget.widget_change_frame_work_hub.slider_change_frame.setValue(5)
        qtbot.mouseClick(main_w.qc_main_widget.widget_change_frame_work_hub.btn_save_settings, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='07', message_key=None,
                                           message_payload=['21', '0003e828', '38', '003c'])
        qtbot.waitUntil(lambda: "19 0c" not in str(socket.messages_heap))
        assert main_w.qc_main_widget.widget_change_frame_work_hub.work_hub_qc.get_parameters('38') == '003c'

    def test_change_color(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa,
                                         mock_qr_scanner_main_w):

        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # test change color to black
        layout = main_w.qc_main_widget.widget_changed_color.layout()
        for i in range(layout.count()):
            color_btn = layout.itemAt(i).widget()

            if "black" in color_btn.text():
                qtbot.mouseClick(color_btn, QtCore.Qt.LeftButton)

        assert main_w.qc_main_widget.label_color_device.text() == 'black'

    def test_change_wifi(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa,
                                         mock_qr_scanner_main_w):

        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        main_w.qc_main_widget.widget_change_wifiname.show()
        main_w.qc_main_widget.widget_change_wifiname.line_edit_name.setText("Corona")
        qtbot.mouseClick(main_w.qc_main_widget.widget_change_wifiname.push_btn_accept, QtCore.Qt.LeftButton)
        assert main_w.qc_main_widget.label_wifi_hub_plus.text() == 'Corona'

    def test_send_report(self, qtbot, mock_socket_csa, mock_telegram_post):

        main_w, server, socket = mock_socket_csa

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        main_w.widget_report_problem.show()
        assert main_w.widget_report_problem.isActiveWindow()

        main_w.widget_report_problem.set_operator("yura")
        main_w.widget_report_problem.line_edit_title.setText("Problem")
        main_w.widget_report_problem.text_browser_problem.setText("Big problem")

        qtbot.mouseClick(main_w.widget_report_problem.push_btn_send, QtCore.Qt.LeftButton)
        assert not main_w.widget_report_problem.isActiveWindow()
        time.sleep(4)
        data = requests.post(url='https://api.telegram.org/bot827080207:AAFfwMFXTSpW4InVR9kNn692hXX2wokyJQM/',
                             data=None)
        assert 'yura' in data
        assert 'Problem' in data
        assert 'Big problem' in data





