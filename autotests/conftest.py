import random
import requests
import json
import pytest
import sqlalchemy
import logging
from collections import deque
from PyQt5 import QtCore
from utils.utils import *
from app_setting import AppSetting
from qc.view_device.device.device_abstract_view import DeviceAbstractView
from qc.view_device.central.central_abstract_view import CentralAbstractView
from qc.view_device.central.central_hub_view import CentralHubView
from qc.window.qc_main_widget import QcMainWidget
from qc.window.login_wiget import LoginWidget
from qc.security.security import KeyDatabase
from csa_client_qt import csa_client
from qc.componets.defect_widget import DefectWidget
from qc.database import database_production
from qc.plugins.check_last_stages import CheckLastStage
from qc.main import QcMainWindow
from pymysql.err import OperationalError
from csa_client_qt.csa_message.csa_message import CsaMessage
from autotests.utils.server import CsaServer

logger = logging.getLogger(AppSetting.LOGGER_NAME)

cred_test_old_db = {
    "host": "0.0.0.0",
    'user': 'root',
    "password": '123',
    "database": "debug_production",
    "port": 3311
}

cred_test_new_db = {
    "host": "0.0.0.0",
    'user': 'root',
    "password": '1234',
    "db_name": "debug_production",
    "port": 3312
}

cred_test_model_csa = {
    'host': "0.0.0.0",
    "user": 'root',
    "password": '12345',
    "db": "debug_models_csa",
    "port": 3313
}


@pytest.fixture(scope='session')
def db():

    try_connect_bd(host='0.0.0.0', user='root', password='1234', db='debug_production', port=3312)

    try_connect_bd(host='0.0.0.0', user='root', password='12345', db='debug_models_csa', port=3313)

    try_connect_bd(host='0.0.0.0', user='root', password='123', db='debug_production', port=3311)

    with open('./autotests/options.json') as j:
        options = json.load(j)
    c = Connector()
    c.init(**cred_test_new_db)
    try:
        create_user(c, 'armen', 'yu', 'ra', '123', {'qc': 1}, active=1)
        create_options(c, 'QC', options=options['options'], operator='armen')
    except sqlalchemy.exc.IntegrityError as ex:
        pytest.skip(str(ex))


class MockSocketCSA(QtCore.QObject):
    connected = QtCore.pyqtSignal()
    disconnected = QtCore.pyqtSignal()
    error = QtCore.pyqtSignal()

    def __init__(self, client_id):
        super(MockSocketCSA, self).__init__()
        self.client_id = client_id
        self.link = 0
        self.message_number = 0
        self.server = CsaServer()
        self._create_default_params()
        self.dev = None
        self.messages_heap = deque(maxlen=100)
        self.messages_heap_from_client = deque(maxlen=100)

        self._task_ack_message_wait = deque(maxlen=25)
        self._task_message_wait = deque(maxlen=25)

    def check_message_exists(self, message_type, message_key, message_payload=''):
        if not message_payload:
            for message in self.messages_heap_from_client:
                if message.message_type == message_type and message.message_key == message_key:
                    self.messages_heap_from_client.remove(message)
                    return True
            return False

        else:
            for message in self.messages_heap_from_client:
                if message.message_type == message_type and message.message_key == message_key and message.payload == message_payload:
                    self.messages_heap_from_client.remove(message)
                    return True
            return False

    def start_send_ping(self):
        pass

    def stop_send_ping(self):
        pass

    def _create_default_params(self):
        self.link = 0
        self.message_number = 0

    def get_message_number(self) -> int:
        self.message_number += 1
        return self.message_number

    def get_link(self) -> int:
        self.link += 1
        if self.link == 255:
            self.link = random.randint(1, 254)
        return self.link

    def send_message(self, message: CsaMessage, timeout_wait_answer=30):
        logger.info(message)
        self.messages_heap_from_client.append(message)
        answer = self.server.message_handler(message)
        if not answer:
            return
        self.messages_heap.append(answer)
        return answer

    def create_task_wait_messa(self, message_number, link, timeout_wait_message):
        if timeout_wait_message == 32:
            m = self.server.obj_settings_answer(self.dev)
            logger.info(m)
            self.messages_heap.append(m)
            return m

    def cancel_all_task(self):
        pass

    def _cancel_task_by_link(self, link):
        pass

    def _write_message(self, message_csa: CsaMessage, timeout_ack, link=None):
        message_flag = 0
        if not link:
            link = self.get_link()
        message_csa.set_message_link(link)
        self.messages_heap_from_client.append(message_csa)

        message_csa.set_message_number(self.get_message_number())
        message_csa._message_flag = "{:02x}".format(message_flag)
        self.__write_message(message_csa)

        result_wait_ack = self.server.message_handler(message_csa)
        self.messages_heap.append(result_wait_ack)
        return result_wait_ack

    def connectToHost(self, *args):
        pass

    def waitForConnected(self):
        return True

    def send_ack_message(self, message: CsaMessage):
        ack_message = CsaMessage()
        ack_message.init_manual(
            sender=self.client_id,
            receiver='00000000',
            message_type='16',
            message_key='00',
            payload=[message._message_number]
        )
        ack_message.set_message_link(self.get_link())
        ack_message.set_message_number(self.get_message_number())
        ack_message._message_flag = '00'
        self.__write_message(ack_message)

    def __write_message(self, message):
        logger.info(message)


@pytest.fixture(scope='module')
def mock_you_sure(monkeypatch_module_scope):
    def _you_sure(*args):
        return True

    monkeypatch_module_scope.setattr(DeviceAbstractView, "_you_sure", _you_sure)


@pytest.fixture(scope='module')
def mock_sim_ok(monkeypatch_module_scope):
    def sim_ok(*args):
        return

    monkeypatch_module_scope.setattr(CentralAbstractView, "show_notification_sim_slot", sim_ok)


@pytest.fixture(scope='module')
def mock_you_sure_central(monkeypatch_module_scope):
    def _you_sure(*args):
        return True

    monkeypatch_module_scope.setattr(CentralAbstractView, "_you_sure", _you_sure)


@pytest.fixture()
def mock_retry_test_popup_false(monkeypatch):
    def retry_test_device_qc(*args):
        return False

    monkeypatch.setattr(CheckLastStage, "retry_test_device_qc", retry_test_device_qc)


@pytest.fixture()
def mock_retry_test_popup_true(monkeypatch):
    def retry_test_device_qc(*args):
        return True

    monkeypatch.setattr(CheckLastStage, "retry_test_device_qc", retry_test_device_qc)


@pytest.fixture()
def mock_hub_connect_server(monkeypatch):
    def _wait_hub_connect2server(*args):
        return

    monkeypatch.setattr(CentralHubView, "_wait_hub_connect2server", _wait_hub_connect2server)


@pytest.fixture()
def mock_share_pro_acc(monkeypatch, mock_socket_csa):
    def share(*args):
        main_w, *_ = mock_socket_csa
        pro_acc = 'rccom.r@ajax.systems'
        group = '1818'
        if not main_w.qc_main_widget.view_obj[0]._share_hub_on_pro_account(hub_id=args[1], email_pro=pro_acc):
            return False
        return group, pro_acc

    monkeypatch.setattr(CentralAbstractView, "share_hub", share)


@pytest.fixture()
def mock_defects_grade(monkeypatch):
    def get_defects_grade(*args):
        return ['Сколи', 'Прожоги']

    monkeypatch.setattr(DefectWidget, "get_defects_grade", get_defects_grade)


@pytest.fixture()
def mock_defects_return(monkeypatch):
    def get_defects_return(*args):
        return ['Відсутній QR на платі', 'Відсутність батарейки/акумулятора']

    monkeypatch.setattr(DefectWidget, "get_defects_return", get_defects_return)


@pytest.fixture()
def mock_return_defect():
    def get_defect_return(*args):
        return ['Відсутній QR на платі', 'Відсутність батарейки/акумулятора']
    yield get_defect_return


@pytest.fixture()
def mock_socket_csa(monkeypatch, main_window):
    class FTP:

        @staticmethod
        def connect(host, timeout):
            return None

        def login(*args):
            return None

        @staticmethod
        def retrbinary(cmd, callback):
            file_name = cmd.split("/")[-1]
            with open(f'./autotests/photos/{file_name}', 'rb') as pic:
                callback(pic.read())

    monkeypatch.setattr(main_window.qc_main_widget.view_obj[10].thread_get_link_image, 'ftp_obj', FTP)

    c = MockSocketCSA("043abb4a")

    monkeypatch.setattr(main_window.qc_main_widget.csa_client, "_socket", c)
    hubs = c.server.get_hub_list()
    c.messages_heap.append(hubs)
    main_window.qc_main_widget.csa_client.connect()
    main_window.qc_main_widget.csa_client.connect_with_email()
    main_window.qc_main_widget.csa_client.timer_handler_message.setInterval(20)
    yield main_window, c.server, c
    del main_window
    del c.server
    del c
    time.sleep(.5)


@pytest.fixture()
def login_widget(db, mock_cred_db, mock_main_w_change_widget):
    main = QcMainWindow()
    main.login_widget.show()
    yield main.login_widget
    main.login_widget.close()
    del main


@pytest.fixture(scope='module')
def mock_main_w_change_widget(monkeypatch_module_scope):
    def change_widget(*args):
        pass

    monkeypatch_module_scope.setattr(QcMainWindow, "change_main_widget", change_widget)


@pytest.fixture()
def main_window(mock_cred_db, monkeypatch):
    monkeypatch.setattr(csa_client, 'SocketCsa', MockSocketCSA)

    main = QcMainWindow()
    main.change_main_widget("armen", "yu ra")
    yield main


class MonkeyPatchScanner(QtCore.QObject):
    readyRead = QtCore.pyqtSignal()

    def setBaudRate(self, *args, **kw):
        pass

    @staticmethod
    def readLine(*args):
        return QtCore.QByteArray(b'armen 123')

    @staticmethod
    def isOpen():
        return True

    def close(self):
        pass

    @staticmethod
    def canReadLine():
        return True

    @staticmethod
    def portName():
        return "ACM0"

    def setPortName(self, port_name):
        pass

    def open(self, *args):
        return True

    def readAll(self):
        pass

    @staticmethod
    def return_qr(qr):
        return QtCore.QByteArray(bytes(qr))


@pytest.fixture()
def mock_qr_scanner_login_widget(monkeypatch, login_widget):
    s = MonkeyPatchScanner()
    monkeypatch.setattr(login_widget, "scanner_login", s)
    login_widget.scanner_login.readyRead.connect(login_widget._handler_data_scanner_port)
    yield s


@pytest.fixture()
def mock_qr_scanner_main_w(monkeypatch, main_window):
    s = MonkeyPatchScanner()
    # s.readLine = return_door_qr
    monkeypatch.setattr(main_window.qc_main_widget, "scanner_qr_code", s)
    main_window.qc_main_widget.scanner_qr_code.readyRead.connect(main_window.qc_main_widget._handler_scanner_qr_code)
    yield s
    del s


@pytest.fixture(scope='module')
def monkeypatch_module_scope():
    from _pytest.monkeypatch import MonkeyPatch
    mpatch = MonkeyPatch()
    yield mpatch
    mpatch.undo()


@pytest.fixture(scope='module')
def mock_cred_db(monkeypatch_module_scope):
    def keys_mock_old_db():
        return cred_test_old_db

    def keys_mock_ndb():
        return cred_test_new_db

    def keys_mock_csa_db():
        return cred_test_model_csa

    monkeypatch_module_scope.setattr(KeyDatabase, "get_keys_production", keys_mock_ndb)
    monkeypatch_module_scope.setattr(KeyDatabase, "get_keys_model_csa", keys_mock_csa_db)
    monkeypatch_module_scope.setattr(KeyDatabase, "get_old_keys_production", keys_mock_old_db)


@pytest.fixture()
def mock_db_authorize_raise_operational_error(monkeypatch):
    def authorize(*args):
        raise OperationalError

    monkeypatch.setattr(database_production.DataBaseProduction(db_keys=KeyDatabase.get_keys_production(), old_db_keys=KeyDatabase.get_old_keys_production()),
                        'authorize', authorize)


@pytest.fixture()
def mock_login_w_scanner_ports(login_widget, monkeypatch):
    ports = ['ttyUSB0', 'ttyUSB1', 'ttyACM0', 'ttyS0']
    ports = sorted([port for port in ports])

    def update_ports_scanner(*args):
        login_widget.combo_box_ports.clear()
        login_widget.combo_box_ports.addItems(ports)

    monkeypatch.setattr(LoginWidget, "update_ports_scanner", update_ports_scanner)


@pytest.fixture()
def mock_main_w_scanner_ports(main_window, monkeypatch):
    ports = ['ttyUSB0', 'ttyUSB1', 'ttyACM0', 'ttyS0']
    ports = sorted([port for port in ports])

    def init_scanner_port(*args):
        main_window.qc_main_widget.comboBox_ports_scanner.clear()
        main_window.qc_main_widget.comboBox_ports_scanner.addItems(ports)

    monkeypatch.setattr(QcMainWidget, "init_scanner_ports", init_scanner_port)


@pytest.fixture(scope='module')
def mock_telegram_post(monkeypatch_module_scope):
    def post(url, data, lst=[]):
        if data:
            lst.append(data['text'])
        assert url == 'https://api.telegram.org/bot827080207:AAFfwMFXTSpW4InVR9kNn692hXX2wokyJQM/'
        return lst

    monkeypatch_module_scope.setattr(requests, "post", post)
