import logging
import re
import time
from abc import ABC, abstractmethod
from PyQt5 import QtCore
from app_setting import AppSetting
from autotests.utils import db_handler

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class AbstractTestViewHub(ABC):
    hub_type = None
    hub_id = None
    hub_type_num = None

    @abstractmethod
    def test_success_hub_reg(self, *args):
        pass

    @abstractmethod
    def test_scan_hub_without_prog(self, *args):
        pass

    @abstractmethod
    def test_scan_hub_without_assembling(self, *args):
        pass

    @abstractmethod
    def test_hub_with_qc_passed(self, *args):
        pass

    @abstractmethod
    def test_success_hub_reg_with_grade_defects(self, *args):
        pass

    @abstractmethod
    def test_all_func_fail(self, *args):
        pass

    @abstractmethod
    def test_tamper_fail(self, *args):
        pass

    @staticmethod
    def add_hub_last_steps(database, region="EU", programming=False,
                                                   assembling=False,
                                                   long_test=False,
                                                   qc=False):

        database.delete_central_from_db()
        database.insert_central_in_db(region=region)
        database.insert_central_in_csa_db()
        if programming:
            database.insert_central_prog()
        if assembling:
            database.insert_central_assembler()
        if long_test:
            database.insert_central_long_test()
        if qc:
            database.insert_central_qc()

    @staticmethod
    def wait_freq_apply(qtbot, main_instance):
        with qtbot.waitSignal(signal=main_instance.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=5 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_instance.qc_main_widget.label_frequency_device.text()).group(0)

    @staticmethod
    def change_freq_to_ru(qtbot, mock_socket):
        main_w, _, socket = mock_socket
        for button in main_w.qc_main_widget.widget_changed_frequency.buttons_freq:
            if "RU" in button.text():
                freq_ru = button
                assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                                main_w.qc_main_widget.label_frequency_device.text()).group(0)

                qtbot.mouseClick(freq_ru, QtCore.Qt.LeftButton)
                assert socket.check_message_exists(message_type='0c', message_key='03')
                assert re.match(r'RU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                                main_w.qc_main_widget.label_frequency_device.text()).group(0)

    def scan_and_test_default_setup(self, main_instance, view_obj_num):
        main_instance.qc_main_widget.handler_qr_code(db_handler.hubs[self.hub_type][0])
        qr_code = db_handler.hubs[self.hub_type][0].replace("-", '')

        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 0).label_id.text() == qr_code[:8]
        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 0).label_master_key.text() == qr_code[8:16]
        assert not main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).defects_popup.get_defects_return() == []

    def scan_without_prog(self, qtbot, mock_socket_csa, database):
        time.sleep(1)
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=database)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.hubs[self.hub_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап програмування та первиного " \
                                                                             "тестування не пройдений, " \
                                                                                "поверніть його на відповідний етап!"

    def scan_without_assembling(self, qtbot, mock_socket_csa, database):
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=database, programming=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.hubs[self.hub_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап зборки непройдений, поверніть " \
                                                                             "пристрій на відповідний етап!"

    def scan_without_long_test(self, qtbot, mock_socket_csa, database):
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.hubs[self.hub_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап лонг теста не пройдений, " \
                                                                             "поверніть пристрій на відповідний етап!"

    def scan_hub_with_qc_passed(self, qtbot, main_w, database):
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=database, programming=True,
                                                   assembling=True,
                                                   long_test=True,
                                                   qc=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.hubs[self.hub_type][0])

    @staticmethod
    def add_grade_defects(qtbot, main_w, view_obj_num):
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).check_box_grade_defect,
                         QtCore.Qt.LeftButton)


