import re
from PyQt5 import QtCore
from app_setting import AppSetting
from qc.enum_param_where_hub import EnumParamWhereHub
from qc.enum_color import ColorResult
from autotests.central_tests.abstract_test_view_hub import AbstractTestViewHub
from autotests.utils import db_handler
from __version__ import __version__


class TestHubPlus(AbstractTestViewHub):
    AppSetting.DEBUG = True

    hub_type = 'hub_plus'
    hub_id = db_handler.hubs[hub_type][0][:9].replace("-", '')
    hub_type_num = "21"

    database = db_handler.DBHandlerHub(hub_type)

    def test_success_hub_reg(self, qtbot, mock_socket_csa, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        assert main_w.qc_main_widget.get_mode_hub() == EnumParamWhereHub.Standart
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Russian].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Standart].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount].isEnabled()

        self.scan_and_test_default_setup(main_w, view_obj_num=1)
        
        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).
                        push_btn_connect.styleSheet() == ColorResult.success.value)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).
                        styleSheet() == ColorResult.success.value, timeout=5*1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).
                        styleSheet() == ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12), QtCore.Qt.LeftButton)

        assert socket.check_message_exists(message_type='11', message_key='07')
        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 12) is None, timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5*1000)
        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        # check info in central_qc
        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207104
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1715
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info update in central_register
        assert country is None
        assert pro_acc is None

    def test_success_hub_reg_ru(self, qtbot, mock_socket_csa, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, region="RU", programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)
        self.change_freq_to_ru(qtbot, mock_socket_csa)
        assert main_w.qc_main_widget.get_mode_hub() == EnumParamWhereHub.Russian
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Russian].isEnabled()
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Standart].isEnabled()
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland].isEnabled()
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia].isEnabled()
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount].isEnabled()

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).
                        push_btn_connect.styleSheet() == ColorResult.success.value)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).
                        styleSheet() == ColorResult.success.value, timeout=5*1000)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"

        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).
                        styleSheet() == ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9).label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12), QtCore.Qt.LeftButton)

        assert socket.check_message_exists(message_type='11', message_key='07')
        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 12) is None, timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207105
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1815
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country == "RU"
        assert pro_acc is None

    def test_success_hub_reg_it(self, qtbot, mock_socket_csa, mock_hub_connect_server, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)
        qtbot.mouseClick(main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia], QtCore.Qt.LeftButton)
        assert main_w.qc_main_widget.get_mode_hub() == EnumParamWhereHub.Italia
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Russian].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Standart].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount].isEnabled()

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value, timeout=5*1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)

        assert socket.check_message_exists(message_type='11',
                                           message_key='07')

        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        assert socket.check_message_exists(message_type='1e',
                                           message_key='00',
                                           message_payload=['7365742030303033316465352032302020'])

        assert socket.check_message_exists(message_type='1e',
                                           message_key='00',
                                           message_payload=['7365742030303033316465352037662020'])

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 12) is None, timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5*1000)
        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207104
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1715
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country == 'IT'
        assert pro_acc is None

    def test_success_hub_reg_ie(self, qtbot, mock_socket_csa, mock_hub_connect_server, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Russian].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Standart].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount].isEnabled()

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)
        qtbot.mouseClick(main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland], QtCore.Qt.LeftButton)
        assert main_w.qc_main_widget.get_mode_hub() == EnumParamWhereHub.Ireland

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value, timeout=5*1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)

        assert socket.check_message_exists(message_type='11', message_key='07')

        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        assert socket.check_message_exists(message_type='1e',
                                           message_key='00',
                                           message_payload=['7365742030303033316465352032302020'])

        assert socket.check_message_exists(message_type='1e',
                                           message_key='00',
                                           message_payload=['7365742030303033316465352037662020'])

        assert socket.check_message_exists(message_type='1e',
                                           message_key='00',
                                           message_payload=['7365742030303033316465352037332031'])

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 12) is None, timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5*1000)
        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207104
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1718
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country == 'IE'
        assert pro_acc is None

    def test_success_hub_reg_on_pro_acc(self, qtbot, mock_socket_csa, mock_share_pro_acc, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)
        assert not main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Russian].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Standart].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia].isEnabled()
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount].isEnabled()

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)
        qtbot.mouseClick(main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount], QtCore.Qt.LeftButton)
        assert main_w.qc_main_widget.get_mode_hub() == EnumParamWhereHub.ProAccount

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value, timeout=5*1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 12) is None, timeout=5 * 1000)

        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207104
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1818
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country is None
        assert pro_acc == 'rccom.r@ajax.systems'

    def test_scan_hub_without_prog(self, qtbot, mock_socket_csa):
        self.scan_without_prog(qtbot, mock_socket_csa, self.database)

    def test_scan_hub_without_assembling(self, qtbot, mock_socket_csa):
        self.scan_without_assembling(qtbot, mock_socket_csa, self.database)

    def test_scan_without_long_test(self, qtbot, mock_socket_csa):
        self.scan_without_long_test(qtbot, mock_socket_csa, self.database)

    def test_hub_with_qc_passed(self, qtbot, mock_socket_csa, mock_retry_test_popup_true):
        main_w, *_ = mock_socket_csa
        self.scan_hub_with_qc_passed(qtbot, main_w, self.database)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 1) is not None

    def test_success_hub_reg_with_grade_defects(self, qtbot, mock_socket_csa, mock_defects_grade, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        self.add_grade_defects(qtbot=qtbot, main_w=main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 12) is None, timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '1'
        assert __version__ in info
        assert defects == '"Сколи;Прожоги"'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207104
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1715
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country is None
        assert pro_acc is None

    def test_tamper_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Тампер"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_sim_slot1_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Сім холдери"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_sim_slot2_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Сім холдери"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_color_check_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "02")
        ))
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Колір цетралі не співпадає"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_power_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Живлення 220"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_firmware_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c12')
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Версія ПЗ"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_channels_fail_only_eth(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Канали звязку"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_channels_fail_only_gsm(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "04")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Gsm"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Канали звязку"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_wifi_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "0064")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '100'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Wifi"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_all_func_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert "Wifi" in reason
        assert "Рівень батареї" in reason
        assert "Канали звязку" in reason
        assert "Версія ПЗ" in reason
        assert "Живлення 220" in reason
        assert "Колір цетралі не співпадає" in reason
        assert "Сім холдери" in reason
        assert "Тампер" in reason
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_batt_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=1)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "02"
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
            timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # check firmware
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).label_status.text() == "Невідомо"
        test_hub = main_w.qc_main_widget.view_obj[1].client_csa.get_hub_obj(self.hub_id)
        test_hub.set_parameters('37', '00032c81')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[1].cellWidget(0, 6).styleSheet() ==
                                ColorResult.success.value, timeout=5 * 1000)

        # test power hub
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == "Відключене"
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("03", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).label_status.text() == 'Підключене'
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test hub batt lvl
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == 'Невідомо'

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("01", "005e")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 8).label_status.text() == '94'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 7).styleSheet() == ColorResult.success.value

        # test find wifi
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 10),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='0a', message_key='00')
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                       label_second_sim.styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 9). \
                   label_second_sim.styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[1].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[1].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Рівень батареї"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None
