import pymysql
import json
import datetime

connect = ('0.0.0.0', 'root', '1234', 'debug_production', 3312)
connect_csa = ('0.0.0.0', 'root', '12345', 'debug_models_csa', 3313)

devices = {
    "door": ("444321011", "1"),
    "door_plus": ("1122330f1", "15"),
    "motion": ("112234021", "2"),
    "motion_plus": ("1122350e1", "14"),
    "glass": ("112236041", "4"),
    "combi": ("112237081", "8"),
    "mpo": ("112238131", "19"),
    "mpc": ("112239061", "6"),
    "fire_protect": ("112240031", "3"),
    "fire_protect_plus": ("112241091", "9"),
    "leaks": ("112242051", "5"),
    "home_siren_v4": ("112243151", "21"),
    "street_siren": ("112244141", "20"),
    "space": ("1122450b1", "11"),
    "motion_cam_v7": ("1122460d1", '13'),
    "socket": ("1122471e1", '30'),
    "relay": ("112248121", '18'),
    "wall_switch": ("1122491f1", '31'),
    "transmitter": ("112250111", '17'),
    "key_pad": ("1122510a1", '10'),
    "range": ("112252071", '7'),
    "panic_button": ("1122530c1", '12')
}

hubs = {
    "hub": ("00031-de456-348ee-11100", '33'),
    "hub_plus": ("00031-de557-348ee-11200", '33'),
    "hub2": ("00031-de658-348ee-11500", '33'),
    "hub_yavir": ("00031-de759-348ee-11300", '33'),
    "hub_yavir_plus": ("00031-de860-348ee-11400", '33'),
    "oc_bridge": ("FF3456", '255'),
    "uart_bridge": ("FF3457", '254')
}

color = {
    "1": "white",
    "2": "black"
}


class DBHandler:
    def __init__(self, db_keys, dev_type):
        self.dev_type = dev_type
        self.db_keys = db_keys
        self.conn = None

    def __enter__(self):
        self.conn = pymysql.connect(*connect)
        self.cur = self.conn.cursor()
        return self.conn, self.cur

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cur.close()
        self.conn.close()

    def insert_device_in_db(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_register (qr, region, dev_id, board_name, dev_type, color) " \
                  "VALUES (%s, %s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], "EU", devices[self.dev_type][0][:-3], self.dev_type,
                   devices[self.dev_type][1], color[devices[self.dev_type][0][-1]])
            cur.execute(sql, val)
            conn.commit()

    def insert_device_prog(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_prog (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], "armen", "1", datetime.datetime.now(), "{}")
            cur.execute(sql, val)
            conn.commit()

    def insert_device_assembler(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_assembling (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), "{}")

            cur.execute(sql, val)
            conn.commit()

    def insert_device_calibration(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_calibration (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), "{}")

            cur.execute(sql, val)
            conn.commit()

    def insert_device_long_test(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_long_test (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), "{}")

            cur.execute(sql, val)
            conn.commit()

    def insert_device_test_room(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_test_room (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), "{}")

            cur.execute(sql, val)
            conn.commit()

    def insert_device_qc(self):
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_qc (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), "{}")

            cur.execute(sql, val)
            conn.commit()

    def delete_device_from_db(self):
        with self as (conn, cur):
            try:
                tables = ['dev_pack', "dev_qc", "dev_long_test", "dev_test_room", "dev_calibration", "dev_assembling",
                          "dev_prog", "dev_repair", "dev_register"]
                for table in tables:
                    sql = "DELETE FROM debug_production.{0} WHERE qr = %s".format(table)
                    val = (devices[self.dev_type][0],)
                    cur.execute(sql, val)
                conn.commit()

            except Exception as ex:
                print(ex)

    def delete_all_tables_info(self):
        with self as (conn, cur):
            try:
                tables = ['dev_pack', "dev_qc", "dev_long_test", "dev_test_room", "dev_calibration", "dev_assembling",
                          "dev_prog", "dev_repair", "dev_register", 'central_pack', "central_qc", "central_long_test",
                          "dev_assembling", "central_prog", "central_repair", "central_register"]
                sql = f"SET FOREIGN_KEY_CHECKS = 0"
                cur.execute(sql, )
                conn.commit()
                for table in tables:
                    sql = f"TRUNCATE table {table}"
                    cur.execute(sql, )
                    conn.commit()
                sql = f"SET FOREIGN_KEY_CHECKS = 1"
                cur.execute(sql, )
                conn.commit()

            except Exception as ex:
                print(ex)

    def get_stat_device_after_success_reg(self, qr):
        with self as (_, cur):
            sql = "SELECT * FROM dev_qc WHERE qr = %s"
            val = (qr, )

            cur.execute(sql, val)
            info = cur.fetchall()[0]
            return info

    def get_stat_device_after_fail_reg(self, qr):
        with self as (_, cur):
            sql = "SELECT * FROM dev_qc WHERE qr = %s"
            val = (qr, )

            cur.execute(sql, val)
            info_qc = cur.fetchall()[0]

            sql = "SELECT * FROM dev_repair WHERE qr = %s"
            val = (qr, )

            cur.execute(sql, val)
            info_repair = cur.fetchall()[0]
            return info_qc, info_repair

    def create_json(self, dev_id: str):
        info = {
            "batt_info": {"primary": "2.994, 2.945",
                              "secondary": "2.998, 2.943"},
            "flash_val": 22313,
            "photo_info": [
                {
                    "analyze_results": {
                        "bad_pixels": {
                            "amount": 1505,
                            "result": "true"
                        },
                        "distortion": {
                            "description": "bars width: True (28.63 >= 26.5) bad bar borders: True ([8])",
                            "result": "true"
                        },
                        "focus": {
                            "coeff": 0.45,
                            "coeff_deviation": 0.06,
                            "coeff_main_harmonic": 15,
                            "result": "true"
                        }
                    },
                    "link": f"ftp://127.0.0.1/img_{dev_id}_photo_2019-09-09_13-09-16.jpg",
                    "type": "photo",
                    "valid": "true"
                },
                {
                    "analyze_results": {
                        "bad_pixels": {
                            "amount": 0,
                            "result": "true"
                        },
                        "distortion": {
                            "description": "bars width: True (28.86 >= 26.5) bad bar borders: True ([8])",
                            "result": "true"
                        },
                        "focus": {
                            "coeff": 0.45,
                            "coeff_deviation": 0.09,
                            "coeff_main_harmonic": 15,
                            "result": "true"
                        }
                    },
                    "link": f"ftp://127.0.0.1/img_{dev_id}_ir_2019-09-26_20-32-17.jpg",
                    "type": "ir",
                    "valid": "true"
                }
            ],
            "test_room_ver": "Hub2Calibrator_MCam_RC"
            }
        mcam_info = json.dumps(info)
        with self as (conn, cur):
            sql = "INSERT INTO debug_production.dev_test_room (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), mcam_info)

            cur.execute(sql, val)
            conn.commit()

    def add_device_last_steps_by_qr(self, qr, board_name, programming=False,
                                                          assembling=False,
                                                          calibration=False,
                                                          long_test=False,
                                                          test_room=False,
                                                          qc=False):

        with self as (conn, cur):
            try:
                tables = ['dev_pack', "dev_qc", "dev_long_test", "dev_test_room", "dev_calibration", "dev_assembling",
                          "dev_prog", "dev_repair", "dev_register"]
                for table in tables:
                    sql = "DELETE FROM debug_production.{0} WHERE qr = %s".format(table)
                    val = (qr,)
                    cur.execute(sql, val)
                conn.commit()

                sql = "INSERT INTO debug_production.dev_register (qr, region, dev_id, board_name, dev_type, color) " \
                      "VALUES (%s, %s, %s, %s, %s, %s)"
                val = (qr, "EU", qr[:6], board_name, devices[board_name][1], color["1"])
                cur.execute(sql, val)
                conn.commit()

            except Exception as ex:
                pass

        if programming:
            with self as (conn, cur):
                sql = "INSERT INTO debug_production.dev_prog (qr, operator, success, time, info) " \
                      "VALUES (%s, %s, %s, %s, %s)"
                val = (qr, "armen", "1", datetime.datetime.now(), "{}")
                cur.execute(sql, val)
                conn.commit()

        if assembling:
            with self as (conn, cur):
                sql = "INSERT INTO debug_production.dev_assembling (qr, operator, success, time, info) " \
                      "VALUES (%s, %s, %s, %s, %s)"
                val = (qr, 'armen', "1", datetime.datetime.now(), "{}")

                cur.execute(sql, val)
                conn.commit()

        if calibration:
            with self as (conn, cur):
                sql = "INSERT INTO debug_production.dev_calibration (qr, operator, success, time, info) " \
                      "VALUES (%s, %s, %s, %s, %s)"
                val = (qr, 'armen', "1", datetime.datetime.now(), "{}")

                cur.execute(sql, val)
                conn.commit()

        if long_test:
            with self as (conn, cur):
                sql = "INSERT INTO debug_production.dev_long_test (qr, operator, success, time, info) " \
                      "VALUES (%s, %s, %s, %s, %s)"
                val = (qr, 'armen', "1", datetime.datetime.now(), "{}")

                cur.execute(sql, val)
                conn.commit()

        if qc:
            with self as (conn, cur):
                sql = "INSERT INTO debug_production.dev_qc (qr, operator, success, time, info) " \
                      "VALUES (%s, %s, %s, %s, %s)"
                val = (devices[self.dev_type][0], 'armen', "1", datetime.datetime.now(), "{}")

                cur.execute(sql, val)
                conn.commit()

    def delete_device_by_qr(self, qr):
        with self as (conn, cur):
            tables = ['dev_pack', "dev_qc", "dev_long_test", "dev_test_room", "dev_calibration", "dev_assembling",
                      "dev_prog", "dev_repair", "dev_register"]
            for table in tables:
                sql = "DELETE FROM debug_production.{0} WHERE qr = %s".format(table)
                val = (qr,)
                cur.execute(sql, val)
            conn.commit()


class DBHandlerHub:
    def __init__(self, hub_type):
        self.hub_type = hub_type
        self.conn = None

    def __enter__(self):
        self.conn = pymysql.connect(*connect)
        self.conn_csa = pymysql.connect(*connect_csa)
        self.cur = self.conn.cursor()
        self.cur_csa = self.conn_csa.cursor()
        return self.conn, self.cur, self.conn_csa, self.cur_csa

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cur.close()
        self.conn.close()
        self.cur_csa.close()
        self.conn_csa.close()

    def insert_central_in_db(self, region):
        with self as (conn, cur, *_):
            consts = json.dumps({"imei": "868728038089410", "mac_eth": "38b8ebc2fd86", "central_id": "83758a"})
            sql = "INSERT INTO debug_production.central_register (qr, region, factory_const, central_id, board_name, " \
                  "dev_type, hub_subtype, color) " \
                  "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            val = (hubs[self.hub_type][0], region, consts, hubs[self.hub_type][0][:9].replace("-", ''), self.hub_type,
                   hubs[self.hub_type][1], hubs[self.hub_type][0][-3], color[hubs[self.hub_type][0][-4]])
            cur.execute(sql, val)
            conn.commit()

    def insert_central_in_csa_db(self):
        with self as (*_, conn, cur):
            sql = "INSERT INTO debug_models_csa.hubs (HubId, firmVersion, fwGroup, hubType) " \
                  "VALUES (%s, %s, %s, %s)"
            val = (hubs[self.hub_type][0][:9].replace("-", '').upper(), 208001, "1",
                   hubs[self.hub_type][0][-3])
            cur.execute(sql, val)
            conn.commit()

    def insert_central_prog(self):
        with self as (conn, cur, *_):
            sql = "INSERT INTO debug_production.central_prog (qr, operator, success, time, info) " \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (hubs[self.hub_type][0], "armen", "1", datetime.datetime.now(), "{}")
            cur.execute(sql, val)
            conn.commit()

    def insert_central_assembler(self):
        with self as (conn, cur, *_):
            sql = "INSERT INTO debug_production.central_assembling (qr, operator, success, time, info)" \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (hubs[self.hub_type][0], "armen", "1", datetime.datetime.now(), '{}')
            cur.execute(sql, val)
            conn.commit()

    def insert_central_long_test(self):
        with self as (conn, cur, *_):
            sql = "INSERT INTO debug_production.central_long_test (qr, operator, success, time, info)" \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (hubs[self.hub_type][0], "armen", "1", datetime.datetime.now(), '{}')
            cur.execute(sql, val)
            conn.commit()

    def insert_central_qc(self):
        with self as (conn, cur, *_):
            sql = "INSERT INTO debug_production.central_qc (qr, operator, success, time, info)" \
                  "VALUES (%s, %s, %s, %s, %s)"
            val = (hubs[self.hub_type][0], "armen", "1", datetime.datetime.now(), '{}')
            cur.execute(sql, val)
            conn.commit()

    def delete_central_from_db(self):
        with self as (conn, cur, conn_csa, cur_csa):
            try:
                tables = ["central_qc", "central_long_test", "central_assembling",
                          "central_prog", "central_repair", "central_register"]
                for table in tables:
                    sql = "DELETE FROM debug_production.{0} WHERE qr = %s".format(table)
                    val = (hubs[self.hub_type][0],)
                    cur.execute(sql, val)
                conn.commit()

                sql = "DELETE FROM debug_models_csa.hubs WHERE HubId = %s"
                val = (hubs[self.hub_type][0][:9].replace("-", '').upper(),)
                cur_csa.execute(sql, val)
                conn_csa.commit()

            except Exception as ex:
                print(ex)

    def get_stat_hub_after_success_reg(self, qr):
        with self as (conn, cur, conn_csa, cur_csa):
            sql = "SELECT * FROM central_qc WHERE qr = %s"
            val = (qr, )

            cur.execute(sql, val)
            info = cur.fetchall()[0]

            sql = "SELECT country, pro_account_email FROM central_register WHERE qr = %s"
            val = (qr,)

            cur.execute(sql, val)
            info_register = cur.fetchall()[0]

            sql = "SELECT * FROM hubs WHERE HubId = %s"
            val = (qr[:9].replace("-", ''),)

            cur_csa.execute(sql, val)
            csa_info = cur_csa.fetchall()[0]
            return info, csa_info, info_register

    def get_stat_hub_after_fail_reg(self, qr):
        with self as (conn, cur, conn_csa, cur_csa):
            sql = "SELECT * FROM central_qc WHERE qr = %s"
            val = (qr, )

            cur.execute(sql, val)
            info_qc = cur.fetchall()[0]

            sql = "SELECT * FROM central_repair WHERE qr = %s"
            val = (qr, )

            cur.execute(sql, val)
            info_repair = cur.fetchall()[0]

            sql = "SELECT * FROM hubs WHERE HubId = %s"
            val = (qr[:9].replace("-", ''),)

            cur_csa.execute(sql, val)
            csa_info = cur_csa.fetchall()[0]
            return info_qc, info_repair, csa_info

