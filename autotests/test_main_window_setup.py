import re
import time
from app_setting import AppSetting
from qc.enum_param_where_hub import EnumParamWhereHub


class TestMainWidgetSetup:
    AppSetting.DEBUG = True

    def test_ui_set_default_params(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa):
        main_w, server, _ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # test labels before info is updated
        assert main_w.qc_main_widget.label_status_rssi.text() == 'Невідомий рівень шуму!'
        assert main_w.qc_main_widget.label_frequency_device.text() == 'TextLabel'

        # test ui set username
        assert 'yu ra' in main_w.qc_main_widget.label_operator_qc.text()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=10 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}', main_w.qc_main_widget.label_frequency_device.text()).group(0)

        # assert radio buttons text
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Standart].text() == "Стандартний"
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Russian].text() == "В Росію"
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Italia].text() == "В Італію"
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.Ireland].text() == "В Ірландію"
        assert main_w.qc_main_widget.hubs_destinations[EnumParamWhereHub.ProAccount].text() == "Привязати до аккаунту"

        # test ui current rssi lvl
        with qtbot.waitSignal(signal=main_w.qc_main_widget.timer_update_rssi.timeout, timeout=5 * 1000):
            assert True
        assert re.match(r'.+[-]\d{1,2}.[-]\d{1,2}', main_w.qc_main_widget.label_status_rssi.text()).group(0)

        # test scanner ports combobox
        assert main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()
        assert main_w.qc_main_widget.comboBox_ports_scanner.currentText()

        # test default WiFi
        assert main_w.qc_main_widget.label_wifi_hub_plus.text() == 'AjaxGuest_2G'

        # check pro accs
        pro_accs = ['nedobytko.r@ajax.systems:1818', 'hopsheriff74@gmail.com:1716', 'ajax_1@komkon-kiev.com:1717']
        assert pro_accs == main_w.qc_main_widget._pro_accounts

        # test default color
        assert main_w.qc_main_widget.label_color_device.text() == 'white'

    def test_setup_without_hubs(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa):
        main_w, server, _ = mock_socket_csa

        qtbot.addWidget(main_w.qc_main_widget)

        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_error_change_freq,
                              timeout=10 * 1000):

            main_w.qc_main_widget.csa_client._hubs = []
            main_w.qc_main_widget.widget_changed_frequency.set_default_frequency()
            assert True
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "На аккаунті не знайдено ні одного Хаба! Додайте свій робочий Хаб!"
        time.sleep(2)

    def test_error_change_frequency_on_work_hub(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa):
        main_w, server, _ = mock_socket_csa
        server.change_freq_answer = server.nak_answer

        qtbot.addWidget(main_w.qc_main_widget)

        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_error_change_freq,
                              timeout=10 * 1000):
            assert True
        assert "Невдалось встановити робочу частоту для Хаба!" in main_w.qc_main_widget.err_widget.label_message_text.text()

